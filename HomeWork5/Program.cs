﻿using System;

namespace HomeWork5
{
    class Program
    {
        static void Main(string[] args)
        {
            using (PostgreSQL postgreSQL = new PostgreSQL("Server=localhost; Port=5432; User Id=postgres; Password=postgres;"))
            {
                postgreSQL.CreateDatabase("SomeBank");
                postgreSQL.ConnectToDatabase("Server=localhost; Port=5432; User Id=postgres; Password=postgres; Database=SomeBank;");
                postgreSQL.CreateTables();
                postgreSQL.initializeTables();
                postgreSQL.ShowTablesContent();

                StateController stateController = new StateController(postgreSQL);
                stateController.SelectTable();
            }
        }
    }
}
