﻿using System;
using HomeWork5.States;

namespace HomeWork5
{
    public class StateController : IState
    {
        private PostgreSQL _postgreSQL;
        private IState _state;

        public StateController(PostgreSQL postgreSQL)
        {
            ShowMessege(Environment.NewLine + "ДОБАВЛЕНИЕ СТРОК В ТАБЛИЦЫ БАЗЫ ДАННЫХ.", ConsoleColor.Blue);
            _postgreSQL = postgreSQL;
            _state = new StateSelectTable(this);
        }

        public PostgreSQL postgreSQL => _postgreSQL;

        public (string inputString, ConsoleKey lastInputKey) GetEnteredConsoleValue()
        {
            ConsoleKeyInfo consoleKeyInfo = new ConsoleKeyInfo();
            string inputString = String.Empty;
            
            while (true)
            {
                consoleKeyInfo = Console.ReadKey();

                if (consoleKeyInfo.Key == ConsoleKey.Enter || consoleKeyInfo.Key == ConsoleKey.Escape)
                    break;

                inputString += consoleKeyInfo.KeyChar;
            }
            ShowMessege(inputString, ConsoleColor.Yellow);
            return (inputString: inputString.Trim(), lastInputKey: consoleKeyInfo.Key);
        }

        public void ShowMessege(string message, ConsoleColor color)
        {
            ConsoleColor currentColor = Console.ForegroundColor;
            Console.ForegroundColor= color;
            Console.WriteLine(message);
            Console.ForegroundColor = currentColor;
        }

        public void ChangeState(IState state)
           => _state = state;

        public void SelectTable()
            => _state.SelectTable();

        public void EnterDataIntoTable(string tableName)
            => _state.EnterDataIntoTable(tableName);
    }
}
