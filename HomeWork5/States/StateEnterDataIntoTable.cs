﻿using System;

namespace HomeWork5.States
{
    internal class StateEnterDataIntoTable : IState
    {
        private StateController _stateController;

        public StateEnterDataIntoTable(StateController stateController)
            => _stateController = stateController;

        public void EnterDataIntoTable(string tableName)
        {
            _stateController.ShowMessege("Введите значения нижеуказанных полей через запятую.", ConsoleColor.Green);
            _stateController.ShowMessege(_stateController.postgreSQL.GetTableColumns(tableName), ConsoleColor.Blue);
            _stateController.ShowMessege("Строковые значения и даты вводятся в одинарных кавычках!" + Environment.NewLine +
                                         "Клавиша Esc - возврат к выбору таблицы", ConsoleColor.Green);
            AddDataToTable(tableName);
        }

        private void AddDataToTable(string tableName)
        {
            var enteredValue = _stateController.GetEnteredConsoleValue();
            if (enteredValue.lastInputKey == ConsoleKey.Escape)
            {
                _stateController.ChangeState(new StateSelectTable(_stateController));
                _stateController.SelectTable();
            }
            else
            {
                _stateController.postgreSQL.AddDataToTable(tableName, enteredValue.inputString);
                AddDataToTable(tableName);
            }
        }

        public void SelectTable()
        {
            throw new NotImplementedException();
        }
    }
}
