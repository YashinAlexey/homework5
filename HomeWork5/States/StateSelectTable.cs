﻿using System;

namespace HomeWork5.States
{
    internal class StateSelectTable : IState
    {
        private StateController _stateController;

        public StateSelectTable(StateController stateController)
            => _stateController = stateController;

        public void SelectTable()
        {
            _stateController.ShowMessege("Введите имя таблицы в которую будут добавляться записи." + Environment.NewLine +
                                         "Доступные для ввода имена таблиц: Clients, Accounts, OperationsHistory." + Environment.NewLine +
                                         "Клавиша Esc - выход", ConsoleColor.Green);

            var enteredValue = _stateController.GetEnteredConsoleValue();
            if (enteredValue.lastInputKey == ConsoleKey.Escape)
                Environment.Exit(0);
            else
            {
                bool tableExists = _stateController.postgreSQL.TableExists(enteredValue.inputString);
                if (tableExists)
                {
                    _stateController.ChangeState(new StateEnterDataIntoTable(_stateController));
                    _stateController.EnterDataIntoTable(enteredValue.inputString);
                }
                else
                {
                    _stateController.ShowMessege($"Таблица {enteredValue.inputString} не найдена", ConsoleColor.Red);
                    _stateController.ChangeState(new StateSelectTable(_stateController));
                    _stateController.SelectTable();
                }
            }
        }

        public void EnterDataIntoTable(string tableName)
        {
            throw new NotImplementedException();
        }
    }
}
