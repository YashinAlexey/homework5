﻿using System;
using Npgsql;

namespace HomeWork5
{
    public class PostgreSQL : IDisposable
    {
        private NpgsqlConnection _connection;
        private bool _isDisposed;

        public PostgreSQL(string connectionString)
        {
            _connection = new NpgsqlConnection();
            _connection.ConnectionString = connectionString;
        }
        public void CreateDatabase(string nameDatabase)
        {
            Console.WriteLine("Выполняется создание базы данных");

            NpgsqlCommand command = new NpgsqlCommand(@$"DROP DATABASE IF EXISTS ""{nameDatabase}"";
                                                         CREATE DATABASE ""{nameDatabase}"";", _connection);
            ExecuteCommand(command);            
        }

        public void ConnectToDatabase(string connectionString)
        {
            _connection = new NpgsqlConnection();
            _connection.ConnectionString = connectionString;
        }

        public void CreateTables()
        {
            Console.WriteLine("Выполняется создание таблиц");

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = _connection;
            command.CommandText =
            @"CREATE TABLE ""Clients""
            (
                ""Id"" SERIAL PRIMARY KEY,
                ""FirstName"" CHARACTER VARYING(30),
                ""LastName"" CHARACTER VARYING(30),
                ""Age"" INTEGER
            );

            CREATE TABLE ""Accounts""
            (
                ""Id"" SERIAL PRIMARY KEY,
                ""OpeningDate"" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                ""Amount"" DOUBLE PRECISION,
                ""ClientId"" INTEGER
            );

            CREATE TYPE TYPE_OF_OPERATION AS ENUM ('Debit', 'Credit');

            CREATE TABLE ""OperationsHistory""
            (
                ""Id"" SERIAL PRIMARY KEY,
                ""DateOfOperation"" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                ""TypeOfOperation"" TYPE_OF_OPERATION,
                ""Amount"" DOUBLE PRECISION,
                ""AccountId"" INTEGER
            );";
            ExecuteCommand(command);
        }

        public void initializeTables()
        {
            Console.WriteLine("Выполняется инициализация таблиц");

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = _connection;
            command.CommandText =
            @"INSERT INTO ""Clients"" (""FirstName"", ""LastName"", ""Age"") VALUES
                ('Client A', 'A', 20),
                ('Client B', 'B', 30),
                ('Client C', 'C', 40),
                ('Client D', 'D', 50),
                ('Client E', 'E', 60);

            INSERT INTO ""Accounts"" (""OpeningDate"", ""Amount"", ""ClientId"") VALUES
                ('2020-01-05', 1000, 1),
                ('2020-02-10', 3000, 1),
                ('2020-03-15', 5000, 3),
                ('2020-04-20', 3000, 4),
                ('2020-05-25', 1000, 5);

            INSERT INTO ""OperationsHistory"" (""DateOfOperation"", ""TypeOfOperation"", ""Amount"", ""AccountId"") VALUES
                ('2020-01-11', 'Debit', 100, 1),
                ('2020-01-13', 'Debit', 300, 1),
                ('2020-03-17', 'Credit', 300, 3),
                ('2020-03-19', 'Credit', 500, 3),
                ('2020-05-25', 'Debit', 700, 5),
                ('2020-05-25', 'Credit', 900, 5);";
            ExecuteCommand(command);
        }

        public void ShowTablesContent()
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = _connection;
            command.CommandText =
            @$"SELECT
                TableName,
                Columns,
                Row
            FROM(
                SELECT
                    1 AS SortField,
                    'Clients' AS TableName,
                    'Id; FirstName; LastName; Age' AS Columns,
                    ""Id"" || '; ' || ""FirstName"" || '; ' || ""LastName"" || '; ' || ""Age"" AS Row
                FROM ""Clients""

                UNION

                SELECT
                    2 AS SortField,
                    'Accounts' AS TableName,
                    'Id; OpeningDate; Amount; ClientId' AS Columns,
                    ""Id"" || '; ' || ""OpeningDate"" || '; ' || ""Amount"" || '; ' || ""ClientId"" AS Row
                FROM ""Accounts""

                UNION

                SELECT
                    3 AS SortField,
                    'OperationsHistory' AS TableName,
                    'Id; DateOfOperation; TypeOfOperation; Amount; AccountId' AS Columns,
                    ""Id"" || '; ' || ""DateOfOperation"" || '; ' || ""TypeOfOperation"" || '; ' || ""Amount"" || '; ' || ""AccountId"" AS Row
                FROM ""OperationsHistory""
            ) AS TableData

            GROUP BY ROLLUP (SortField, TableName, Columns, Row)

            ORDER BY SortField, TableName NULLS FIRST, Columns NULLS FIRST, Row NULLS FIRST";

            bool hasRows = false;
            string readValue = String.Empty;

            _connection.Open();
            NpgsqlDataReader reader = command.ExecuteReader();
            hasRows = reader.HasRows;
            if (hasRows)
            {
                object TableName, Columns, Row;

                while (reader.Read())
                {
                    TableName = reader["TableName"];
                    Columns = reader["Columns"];
                    Row = reader["Row"];

                    if (Convert.IsDBNull(TableName)) 
                        continue;
                    else if (Convert.IsDBNull(Columns))
                        Console.WriteLine(Environment.NewLine + "Таблица: " + TableName);
                    else if (Convert.IsDBNull(Row))
                        Console.WriteLine("Колонки: " + Columns);
                    else
                        Console.WriteLine(Row);
                }
            
            }
            _connection.Close();
        }

        public bool TableExists(string tableName)
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = _connection;
            command.CommandText =
            @$"SELECT 1 
               FROM information_schema.tables 
               WHERE table_name = '{tableName}'";
            return ReadData(command).hasRows;
        }

        public string GetTableColumns(string tableName)
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = _connection;
            command.CommandText =
            @$"SELECT ""column_name"" 
               FROM information_schema.columns 
               WHERE table_name = '{tableName}' AND column_name <> 'Id'";
            return ReadData(command).readValue;
        }

        public void AddDataToTable(string tableName, string AddedData)
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = _connection;
            command.CommandText =
            @$"INSERT INTO ""{tableName}"" VALUES
               (default, {AddedData});";
            ExecuteCommand(command);
        }

        private void ExecuteCommand(NpgsqlCommand command)
        {
            _connection.Open();
            command.ExecuteNonQuery();
            _connection.Close();
        }

        private (bool hasRows, string readValue) ReadData(NpgsqlCommand command)
        {
            bool hasRows = false;
            string readValue = String.Empty;

            _connection.Open();
            NpgsqlDataReader reader = command.ExecuteReader();
            hasRows = reader.HasRows;
            if (hasRows)
                while (reader.Read())
                    readValue += (" " + reader.GetValue(0));
            _connection.Close();

            return (hasRows: hasRows, readValue: readValue.Trim());
        }

        protected virtual void Dispose(Boolean isManual)
        {
            if (_isDisposed) return;

            if (isManual) _connection.Dispose();

            _isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        ~PostgreSQL()
        {
            Dispose(false);
        }
    }
}
