﻿
using System;

namespace HomeWork5
{
    public interface IState
    {
        void SelectTable();
        
        void EnterDataIntoTable(string tableName);        
    }
}
